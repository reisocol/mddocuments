# Interview Questions

### Tell me about yourself. (What do you bring to the table?)

I have been making games since I graduated from a master degree in software engineering for Game Development. I helped teams to create web based, mobile games and lastly I participated in a project for VR and AR devices in a multi national company. Using the last engines as Unity and Unreal and I worked in small, medium and recently large teams  across different disciplines and currently I work for AR projects.

My great interest since my masters was to work on Game Console projects and my main focus is to work on features in gameplay programming but I have great interest in game engine modification or a tool that can increase the productivity of the team in specific features.

