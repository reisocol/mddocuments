# C++ Course

## Outline

- Keywords
- Lambda
- Multithreading
- Functors



### Keywords

**auto** 

​	 specifies that the type of the variable that is being declared will be automatically deduced from its initializer.

​	for functions, specifies that the return type will be deduced from its return statements

​	for non-type template parameters, specifies that the type will be deduced from the argument

```
 auto a = 1 + 2;            // type of a is int
 auto b = add(1, 1.2);      // type of b is double
 decltype(auto) c1 = a;
 auto lambda = [](int x) { return x + 3; };
```

**explicit**

​	specifies that a constructor is explicit, cannot be used for implicit conversions (a type is used in context when another type is expected) or copy-initialization (initializes an object from another object)

```
struct B
{
    explicit B(int) { }
    explicit B(int, int) { }
    explicit operator bool() const { return true; }
};

B b1 = 1; // not OK
B b2(2); // OK
```

**union**

​	special class type that can hold only one of its non-static data members at a time

```
union S
{
    std::int32_t n;     // occupies 4 bytes
    std::uint16_t s[2]; // occupies 4 bytes
    std::uint8_t c;     // occupies 1 byte
};
```

**typedef**

​	creates an alias that can be used anywhere in place of a (complex name) type name

```
// simple typedef
typedef unsigned long ulong;

// the following two objects have the same type
unsigned long l1;
ulong l2;

// more complicated typedef
typedef int int_t, *intp_t, (&fp)(int, ulong), arr_t[10];
```

**mutable**

​	appear in any type specifier, to specify constness or volatility of the object being declared . Permits modification of the class member declared mutable even if the containing object is declared const

```
const struct
    {
        int n1;
        mutable int n2;
    } x = {0, 0}; 
x.n1 = 4; // error: member of a const object is const
x.n2 = 4; // ok, mutable member of a const object isn't const
```

**struct**

​	are user-defined types, defined by class-specfier

```
struct MyStruct {
    int value;
    friend std::ostream& operator<<(std::ostream& os, const S& s);
    // definition provided in MyStruct.cpp file which uses #include <ostream>
};
```

**class**

​	forward declaration. Declares a class type which will be defined later in this scope.

```
class Vector; // forward declaration
class Matrix {
    // ...
    friend Vector operator*(const Matrix&, const Vector&);
};
class Vector {
    // ...
    friend Vector operator*(const Matrix&, const Vector&);
};
```

**inline**

​	to serve as a hint for the compiler to perform optimization. Replaces a call of that function with its body.

```
// file test.h
#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED
inline int sum (int a, int b)
{
    return a+b;
}
#endif
```

template



### Lambda

​	Constructs a closure: an unnamed function object capable of capturing variables in scope.

##### 	Syntax

```C++
[ captures ] ( params ) -> ret { body }
```

```
[ captures ] ( params ) { body }
```

```
[ captures ] { body }
```

```
// generic lambda, operator() is a template with two parameters
auto glambda = [](auto a, auto&& b) { return a < b; };
bool b = glambda(3, 3.14); // ok

// generic lambda, operator() is a template with two parameters
auto glambda = []<class T>(T a, auto&& b) { return a < b; };
```

### Pointers

​	variable that stores an address memory, that is, points to a location in memory.

​	pointers are used to enable the programmer to write programs that effectively consume the resources optimally.

​	Memory locations are typically addressed using hexadecimal notation. A number system with base 16. It is usually prefixed by **0x**. For example, 0x10 is the number 10 in hexadecimal.

Pointer declaration

```
 PointedType * PointerVariableName; 
```

#### Best Practices

Initialize a pointer with NULL

```
 PointedType * PointerVariableName = NULL; 
```

Declaring a pointer to an integer

```
 int *pointsToInt = NULL; 
```

Use & to find the address in memory

```
int b;

pointsToInt = &b;
```

Accessing the value data of a pointer with dereference operator (*)

Also called indrection operator.

```
cout << *pointsToInt ;
```

Finding out the size of a pointer

```
cout << sizeof(int*);
```

The amount of memory consumed by a pointer that stores an address is the same regardless of data type.



To be able to program an application to consume memory resources optimally you need to use **dynamic memory allocation**.

Use operators new and delete to allocate and release memory dynamically

```
 Type* Pointer = new Type; 
 delete Pointer;
 
 // Multiple allocations
 Type* Pointer = new Type[numElements]; // allocate a block
delete[] Pointer; // release block allocated above
```

If you don't release the memory allocated, this reduces the memory available and make the application slower. This called a **leak**.

Incrementing and decrementing pointers

```
// Interpreted by the compiler as to point to the next integer as // it is going to increment 4 bytes (the size of int)
Pointer++;

//Pointer occupies 0x002EFB34
//After incrementing 0x002EFB38
```

Using const on pointers

Two combinations:

- address contained in the pointer cannot be changed, data contained in the pointer can be changed

  ```
  int* const pDaysInMonth = &daysInMonth; 
  ```

- value contained in the pointer cannot be changed, pointer can point to another address

  ```
  const int* pDaysInMonth = &daysInMonth; 
  ```

Pointers to Functions

