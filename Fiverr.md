I will teach you how to code your game by visual scripting in blueprints and also working together with C++ code.



Currently, we have 3 games that you can learn how to develop. 



Starter: You are going to learn the basics of blueprints and C++ to develop a puzzle game where you have to turn the lights on by matching the rules at the right time.



Standard: Now you have more control in blueprints to achieve more complex actors that you can show different parts of a complex model like a motorbike.



Premium: A full featured third person action game where you are going to learn how to construct combat gameplay with AI NPC and combo move controls.



Rodrigo Reis