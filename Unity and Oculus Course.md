# Unity and Oculus Course

## VR Game Development and Prototyping



## Pre-Production

### Ideation

Start planning the experience in VR.

##### Mind Mapping

Write the problem/topic you're interested in and write the ideas that help to define topic further.

##### Force connection

Describe related connections among a topic.

#### Audience

Know your device. Use all resources that your device can deliver.

Solid and fun controls

Controls intuitive

##### Gamer segments

Depth profiles on different types of games.

##### Target Audience

Escape Rooms (Puzzles genre)

What are some comparable escape room games?

What are some comparable escape room games in VR?

​	What do they do well?

​	What do they do poorly?

​	What are they missing?

#### Game Design and Features

Questions to discover features in your experience.

What will the player be doing in your game?

How will they do it?

How does the player progress through the game?

How is the narrative delivered?

What types of transitions? Scene loading, etc

Best practices

##### Movement

Confortable 

Oculus quest (no locomotion)

Teleportation (confortable for all players)

Which button will activate it?

Show arrow where the player will be positioned after teleportation.

##### Controls

Actions mapped to different parts of the controls

Show in game actions 

Tutorials 

Help the player to the focus areas where to use the controls

Intuitive controls

Oculus Hand Module (Implement)

#### Building a Game Design Document

What is the game about:?

What will it look like?

What will it feel like?

How will the user interact with the game?

Game overview

Unique selling points

Gameplay scope

Map out game loops

Target art style details

Player profile stories

Milestone schedule

Project cycle:

- Development cycle
- Team composition
- Development costs

Achievable

Jira

Confluence

Trello

Burndown Chart

Marketing Plan:

- Will you be using primarily social media?
- Which platforms and how?
- Will you be targeting specific interest groups?
- Who and how do you plan on reaching them?

#### Development and Prototyping

Module game development (mechanics)

Core interactions independent of another

Specific to VR

Risks Aspects (Prototyping)

##### Unique Selling Points (USPs)

A feature or characteristic of a game or product, that distinguishes it from others of a similar nature and makes it more appealing.

Test in VR

Interact in a physical space

#### Management Tools

Unity Hub

Version Control

Github

Perforce Integration

First Stages of Production:

First Playable

Interim Builds

Vertical Slide



## Production

Enable VR in Unity

Oculus integration and VRTK

Tips for developing VR projects in Unity

