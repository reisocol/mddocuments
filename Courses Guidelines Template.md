# Courses Guidelines Template

### Example

By the end of the series you will (what is going to be able to do?)

### Prerequisites

Make sure you've met the following requirements before doing this tutorial

### Installing (requirements)

Installing any dependencies files to be able to make the tutorial

### Overview

What you are going to do in the tutorial

### Objective

Checkpoints to reach in the tutorial

- Configuring an Unreal project for Hololens development
- Importing assets and setting up a scene
- Creating actors and script-level events with blueprints