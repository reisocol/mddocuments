# C++ Design Patterns

### Abstract Factory Pattern

#### Abstract Classes

Abstract classes declare only pure virtual functions.

A pure virtual function is specified by placing "=0"

##### Example:

```
virtual void CreateVertexBuffer() = 0;
```

The purpose is to provide an appropriate base class from which other classes can inherit.

They cannot be used to instantiate objects and serves only as an **interface**.

Classes that can be used to instantiate objects are called **concrete classes**.

https://iq.opengenus.org/abstract-factory-pattern-cpp/

### Proxy Pattern

Delegate of the real work to some other object.

```
class Subject {
 public:
  virtual void Request() const = 0;
};

class RealSubject : public Subject {
 public:
  void Request() const override {
    std::cout << "RealSubject: Handling request.\n";
  }
};

class Proxy : public Subject {
  /**
   * @var RealSubject
   */
   
 public:
  Proxy(RealSubject *real_subject) : real_subject_(new RealSubject(*real_subject)) 	 {}
  
 private:
  RealSubject *real_subject_;
  
void Request() const override {
    if (this->CheckAccess()) {
      this->real_subject_->Request();
      this->LogAccess();
    }
  }
```

