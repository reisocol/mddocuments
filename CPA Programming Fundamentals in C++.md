

# CPA: Programming Fundamentals in C++

### Introduction

A language is a tool for expressing and recording human thoughts.

A programming language is defined by a certain set of rigid rules, much more inflexible than any natural language.

These rules determine which symbols (letters, digits, punctuation marks, and so on) could be used in the language.

- Lexicon - letters, digits, punctuation marks
- Syntax - Set of rules determines the appropriate ways of collating symbols
- Semantics - Recognize the meaning of every statement expressed in the given language

Any program that is written must be error-free lexically, syntactically and semantically.

A computer responds to a predetermined set of known commands.

- Instruction list - set of known well commands
- Machine Language - Instruction List of computers

What makes a good programmer:

- Programmer's intention
- Imagination
- Knowledge
- Experience

#### High level programming language

Programs written in machine language are very difficult to understand for humans.

An intermediate common language for both humans and computers working together as a bridge between people's language and computer language.

#### Portability

The programs written in high-level languages can be translated into any number of different machine languages, and thus enable them to be used on many different computers.

#### Compiler

Translation from a high level programming language to a machine language is made by a compiler.

This process is called compilation.

#### Source Code

Created in a specialized text editor called IDE.

C++ programs are saved in format **.cpp**

Other suffices exist such as:

- cc
- cp
- cxx
- c++
- c

If your programs does not contain any errors, the compiler will generate a a program in machine language called **executable file**.

Depending on the operating system you are working, executable files are represented as .exe files.

Your source code might be huge and divided among several or even dozens of source files.

In this case, the compiling process splits into two phases: a compilation of your source in order to translate it into machine language and a joining (or gluing) of your executable code with the executable code derived from the other developers into a single and unified product.

This process is called **linking** made by the **linker**.

#### Readings

The C++ Programming Language. Bjarne Stroustrup

Thinking in C++. Bruce Eckel

Jumping into C++. Alex Allain

Effective C++. Scott Meyers

C++ Standard Committee. https://isocpp.org/std

### Writing Programs 

#### Preprocessor

Operations that are performed before the full compilation takes place.

Are fully controlled by its directives.

##### 	Include directive

Writing programs is like building a construction with ready-made blocks.

These blocks are inserted into the program by including **header files**. These files contain a collection of preliminary information about ready-made blocks which can be used by a program. 

In C++ language, an abstract container can hold a logical grouping of unique entities (blocks) which are represented by **namespace**.

```c++
#include <iostream> // include directive
using namespace std; // namespace
int main(void)
{
	cout << "Hello World" << endl;
	return 0;
}
```

##### 	Functions

One most common type of blocks used to build C++ programs.

Imagine a function as a black box where you can insert something into it and take something new out of it.

Things to put into the box are called **function arguments**.

In the C++ language, a function is mandatory and works as the **entry point** of the program. The main function.

##### 	Function Prototypes

Information about function.

- name
- number of arguments
- return type

#####    Function Body

​	All instructions inside { } operators.

​	Each instruction ends with a ; that marks the end of a statement.

​	When writing programs bear in mind that **readability** is a important factor when working with other programmers.

### Numbers

**Binary system** is the way computer store numbers and perform operations on them.

Computers can handle numbers such as:

- Integers - whole numbers
- Float-point numbers - numbers that contain the fractional parts

The characteristic of a number which determines its kind, range and application is called **type**.

- int
- float

Numbers can also be represented as:

- octal - 0123 (83)
- hexadecimal - 0x123 (291)

### Variables

Container to hold values.

- Name
- type
- value

Naming variables:

- upper-case or lower-case Latin digits
- Must begin with letter
- Upper and lower case are treated differently

Declaration

​	is a syntactic structure that binds a name provided by the programmer with a specific type offered by the C++ language

```c++
int variable1, account_balance, invoices;
```

Giving values

Use the assignment operator =

```c++
Counter = 1;
```

Increment and decrement values:

```c++
Counter++;
```

```c++
Counter--;
```

#### Keywords

list of words that play a very special role in every C++ language program

- and
- asm
- auto
- do
- double
- dynamic_cast
- else
- enum
- explicit
- export

#### Comments

//

/* */

```c++
/*************************************************
    Counting sheep version 1.0
    Author: Ronald Sleepyhead, 2012
    email: rs@insomnia.org

    Changes:
    2012-09-13: Ginny Drowsy: counting black sheep improved
 *************************************************/ 
```

### Floating-point Numbers

These are the numbers that have a fractional part after the decimal point.

2.5

.4 or 0.4

#### Scientific Notation

1000000 or 
$$
1*10^6
$$

```c++
float x = 1e6;
```

For very small floating pointers numbers like 
$$
6.62607 * 10^-34
$$

```c++
float x = 6.62607e-34;
```

#### Loss of accuracy

```c++
int i;
float f;
i=100;
f=i;
```

The transformation affects the internal (machine) representation of those values as **computers use different methods for storing \*floats\* and \*ints\* in their memory**.

Implementation dependent issue

```cpp
int i;
float f;
f=1E10;
i=f;
```

#### Operators

* multiplication operator *
* division operator /, 5.0/4.2, dividend by divisor
* division by zero, compilation error, runtime error or some message at runtime
* infinitive values (exception)
* addition operator +
* subtract operator -, unary operator sometimes as in i=-100;
* remainder operator % modulo operation
* Priorities
* Left binding of operations
* Parenthesis (change the natural order of calculation)
* Incrementing ++
* Decrementing --
* Pre and Post operators, ++variable or variable-- and --variable or variable--
* Shortcut operators, i*=2

```cpp
i=i+2*j;
i+=2*j;
```

### Characters

In C++, all strings are treated as arrays.

To store and manipulate characters, the C++ language provides a special type of data. This type is called **char**.

#### ASCII Code

Computers store characters as numbers.

Every character used by computers corresponds to a unique number.

[**ASCII** ](https://en.wikipedia.org/wiki/ASCII)(short for American Standard Code for Information Interchange) is the most widely used and nearly all modern devices (like computers, printers, mobile phones, tablets, etc.) use it. The code allows for 256 different characters, but we’re only interested in the first 128.

ASCII code is being superseded (or rather extended) by a new international standard named UNICODE.

the ASCII set is a [UNICODE](https://en.wikipedia.org/wiki/Unicode) subset.

UNICODE is able to represent virtually all characters used throughout the world.

```cpp
Character = 'A';
Character = 65;
```

